<?php


include_once (ROOT . 'models/Author.php');
include_once (ROOT . 'controllers/AppController.php');

class AuthorController extends AppController {
     
    public function __construct() {
        parent::__construct();
    }
    
    public function actionIndex() {
        
        $author = new Author();
        $authors = $author->getAuthors();
        
        if (empty($authors)) {
            $message = 'Нет авторов';
            $this->view->renderHtml('author/index.php', ['message' => $message]);
            
            return;
        }
 
        $this->view->renderHtml('author/index.php', ['authors' => $authors]);
        
        return true;
    }
    
    public function actionCreate() {
        
        if (empty($_POST)) {
            
            $this->view->renderHtml('author/create.php', [
                'button' => 'Добавить',
            ]);
            
            return true;
        }
        
        $name = $_POST['name'];

        $author = new Author();
        
        $author->create($name);
        
        header('Location: /author');

        return true;
        
    }
    
    public function actionUpdate($id ='') {
        
        if (empty($id)) {

            return false;
        }
        
        $author = new Author();
        
        if (empty($_POST)) {

           $this->view->renderHtml('author/create.php', [
               'button' => 'Обновить',
               'author' => $author->getAuthorById($id),
           ]);

           return true;
        }
        
        $name = $_POST['name'] ?? '';
        
        $author->update($id, $name);
        
        header('Location: /author');

        return true;
    }
    
    public function actionDelete($id = '') {
        
        if (empty($id)) {

            return false;
        }
        
        $author = new Author();
        $author->delete($id);
        
        header('Location: /author');

        return true;
    }
}
