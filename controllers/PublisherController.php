<?php


include_once (ROOT . 'models/Publisher.php');
include_once (ROOT . 'controllers/AppController.php');


class PublisherController extends AppController {
     
    public function __construct() {
        parent::__construct();
    }
    
    public function actionIndex() {
        
        $publisher = new Publisher();
        $result = $publisher->getPublishers();
        
        if (empty($result)) {
            $message = 'Нет издательств';
            $this->view->renderHtml('publisher/index.php', ['message' => $message]);
            return;
        }
     
        $this->view->renderHtml('publisher/index.php', ['publishers' => $result]);
        
        return true;
    }
    
    public function actionCreate() {
        
        if (empty($_POST)) {

           $this->view->renderHtml('publisher/create.php', [
               'button' => 'Добавить',
           ]);

           return true;
        }
        
        $name = $_POST['name'] ?? '';
        $address = $_POST['address'] ?? '';
        $phone = $_POST['phone'] ?? '';

        $publisher = new Publisher();
        $publisher->create($name, $address, $phone);
        
        header('Location: /publisher');

        return true;
        
    }
    
    public function actionUpdate($id ='') {
        
        if (empty($id)) {
            
            header('Location: /publisher');

            return;
        }
        
        $publisher = new Publisher();
        
        if (empty($_POST)) {

           $this->view->renderHtml('publisher/create.php', [
               'button' => 'Обновить',
               'publisher' => $publisher->getPublisherById($id),
           ]);

           return true;
        }
        
        $name = $_POST['name'] ?? '';
        $address = $_POST['address'] ?? '';
        $phone = $_POST['phone'] ?? '';

        
        $publisher->update($id, $name, $address, $phone);
        
        header('Location: /publisher');

        return true;
    }
    
    public function actionDelete($id = '') {
        
        if (empty($id)) {

            return false;
        }
        
        $publisher = new Publisher();
        $publisher->delete($id);
        
        header('Location: /publisher');

        return true;
    }
}
