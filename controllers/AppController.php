<?php

require_once (ROOT . 'components/Db.php');
require_once (ROOT . 'components/View.php');

class AppController {
    
    protected $view;


    public function __construct() {
        
        $this->view = new View(VIEW);
        
    }
    
    
}
