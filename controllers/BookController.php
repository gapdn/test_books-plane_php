<?php


include_once (ROOT . 'models/Book.php');
include_once (ROOT . 'controllers/AppController.php');
include_once (ROOT . 'models/Author.php');
include_once (ROOT . 'models/Catalog.php');
include_once (ROOT . 'models/Publisher.php');
include_once (ROOT . 'models/Photo.php');
include_once (ROOT . 'models/BookToAuthor.php');

class BookController extends AppController {
     
    public function __construct() {
        parent::__construct();
    }
    
    public function actionIndex() {
        
        
        $book = new Book();
        $result = $book->getBooks();
        $photo = new Photo();
        
        if (empty($result)) {
            $message = 'Пока нет книг | ';
            $this->view->renderHtml('book/index.php', ['message' => $message]);
            return;
        }
        
        $books = [];
        foreach ($result as $b) {
            
            $books[] = [
                'id' => $b['id'],
                'name' => $b['name'],
                'photo' => $photo->getPath($b['id']), 
                'authors' => $book->getAuthorsName($b['id']),
                'date_published' => $b['published_at'],
                'catalog' => $book->getCatalogName($b['catalog_id']),
                'publisher' => $book->getPublisher($b['publisher_id'])
            ];
        }

        $this->view->renderHtml('book/index.php', ['books' => $books]);
        
        return true;
    }
    
    public function actionAbout() {
        
        $this->view->renderHtml('about.php');
        
        return true;
    }
    
    public function actionCreate() {
        
        $author = new Author();
        $publisher = new Publisher();
        $catalog = new Catalog();
        $photo = new Photo();
        
        if (empty($_POST)) {
            $this->view->renderHtml('book/create.php', [
                'button' => 'Добавить',
                'authors' => $author->getAuthors() ?? [],
                'publishers' => $publisher->getPublishers() ?? [],
                'catalogs' => $catalog->getCatalogs() ?? [],
            ]);
            
            return true;
        }
        
        $book_name = $_POST['name'] ?? '';
        $author_ids = $_POST['author'] ?? '';
        $date = $_POST['date'] ?? '';
        $catalog_id = $_POST['catalog'] ?? '';
        $publisher_id = $_POST['publisher'] ?? '';
        
        $book = new Book();
        
        $book_to_author = new BookToAuthor();
        $book->create($book_name, $catalog_id, $publisher_id, $date);
        
        
        $b_id = $book->getId($book_name);
        if ($path = $photo->upload($_FILES['photo'])) {
            $path = $photo->preparePath($path);
            $photo->create($path, $b_id);
        } else {
            //TODO: add alert
        }
        
        foreach ($author_ids as $author_id) {
            $book_to_author->create($b_id, $author_id);
        }
        
        header('Location: /');

        return true;
        
    }
    
    public function actionUpdate($id ='') {
        
        if (empty($id)) {
            
            header('Location: /');

            return;
        }
        
        $book = new Book();
        $author = new Author();
        $publisher = new Publisher();
        $catalog = new Catalog();
        $photo = new Photo();
        $book_to_author = new BookToAuthor();
        $book_old = $book->getBookById($id);
        $b_to_as = $book_to_author->getBookToAuthors($id);
        
        $checked_ids = [];
        foreach ($b_to_as as $row) {
            $checked_ids[] = $row['author_id'];
        }
        if (empty($_POST)) {
            
            $this->view->renderHtml('book/create.php', [
                'button' => 'Обновить',
                'book' => $book_old ?? [],
                'publishers' => $publisher->getPublishers() ?? [],
                'authors' => $author->getAuthors() ?? [],
                'catalogs' => $catalog->getCatalogs() ?? [],
                'checked_ids' => $checked_ids ?? [],
            ]);
            
            return true;
        }
        
        $book_name = $_POST['name'] ?? '';        
        $author_ids = $_POST['author'] ?? '';
        $date = $_POST['date'] ?? '';
        $catalog_id = $_POST['catalog'] ?? '';
        $publisher_id = $_POST['publisher'] ?? '';
        
        $book->update($id, $book_name, $catalog_id, $publisher_id, $date);
        if ($path = $photo->upload($_FILES['photo'])) {
            $path = $photo->preparePath($path);
            $photo->update($id, $path);
        } else {
            //TODO: add alert
        }
        
        
        foreach ($author_ids as $a_id) {
            if ($book_to_author->isExists($id, $a_id)) {
                continue;
            }
            $book_to_author->create($id, $a_id);
            
        }
        
        foreach ($b_to_as as $row) {
            
            if(!in_array($row['author_id'], $author_ids)) {
                $book_to_author->delete($row['id']);
            }
        }
        
        header('Location: /');

        return true;
    }
    
    public function actionDelete($id = '') {
        
        if (empty($id)) {

            return false;
        }
        
        $book = new Book();
        $book->delete($id);
        
        $book_to_author = new BookToAuthor();
        $b_t_as = $book_to_author->getBookToAuthors($id);
        foreach ($b_t_as as $row) {
            $book_to_author->delete($row['id']);
        }
        $photo = new Photo();
        $photo->delete($id);
        
        header('Location: /');

        return true;
    }
}
