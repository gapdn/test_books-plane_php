<?php


include_once (ROOT . 'models/Catalog.php');
include_once (ROOT . 'controllers/AppController.php');

class CatalogController extends AppController {
     
    public function __construct() {
        parent::__construct();
    }
    
    public function actionIndex() {
        
        $catalog = new Catalog();
        $catalogs = $catalog->getCatalogs();
        
        if (empty($catalogs)) {
            $message = 'Нет рубрик';
            $this->view->renderHtml('catalog/index.php', ['message' => $message]);
            return;
        }
        $result = [];
        foreach ($catalogs as $c) {
            $result[] = [
                'parent_catalog' => ($c['parent_catalog_id'] != 0) ? $catalog->getName($c['parent_catalog_id']) : '',
                'id' => $c['id'],
                'title' => $c['title']
            ]; 
        }

        $this->view->renderHtml('catalog/index.php', ['catalogs' => $result]);
        
        return true;
    }
    
    public function actionCreate() {
        
        $catalog = new Catalog();
        if (empty($_POST)) {
            $result = $catalog->getCatalogs();
            $this->view->renderHtml('catalog/create.php', [
                'catalogs' => $result,
                'button' => 'Добавить',
            ]);
            
            return true;
        }
        
        $name = $_POST['name'];
        $parent_catalog_id = $_POST['parent'] ?? 0;
        $catalog->create($name, $parent_catalog_id);
        
        header('Location: /catalog');

        return true;
        
    }
    
    public function actionUpdate($id ='') {
        
        if (empty($id)) {

            return false;
        }
        
        $catalog = new Catalog();
        
        if (empty($_POST)) {
            
            $this->view->renderHtml('catalog/create.php', [
                'button' => 'Обновить',
                'catalog' => $catalog->getCatalogById($id),
                'catalogs' => $catalog->getCatalogs() ?? [],
            ]);

           return true;
        }
        
        $title = $_POST['name'] ?? '';
        $parent_catalog_id = $_POST['parent'] ?? 0;
        $catalog->update($id, $title, $parent_catalog_id);
        
        header('Location: /catalog');

        return true;
    }
    
    public function actionDelete($id = '') {
        
        if (empty($id)) {

            return false;
        }
        
        $catalog = new Catalog();
        $catalog->delete($id);
        
        header('Location: /catalog');

        return true;
    }
}
