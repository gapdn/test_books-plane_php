<?php

ini_set('display_errors', 1);

define('COUNT_CHAR_FO_SHORT_CONTENT', 100);
define('COUNT_FAVORITE_POST', 5);
define('ROOT', __DIR__ . '/../');
define('VIEW', __DIR__ . '/../views');
define('UPLOAD_DIR', __DIR__ . '/uploads/');
define('MAX_FILE_SIZE', 500000);

require_once ROOT . 'components/Router.php';
//require_once ROOT . 'components/Db.php';
//echo ROOT; die;

$router = new Router();
$router->run();

