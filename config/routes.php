<?php

return [
    'comment/create' => 'comment/create',
    'book/update/([0-9]+)' => 'book/update/$1',
    'book/delete/([0-9]+)' => 'book/delete/$1',
    'book/create' => 'book/create',
    
    'author/update/([0-9]+)' => 'author/update/$1',
    'author/delete/([0-9]+)' => 'author/delete/$1',
    'author/create' => 'author/create',
    'author' => 'author/index',
    
    'catalog/update/([0-9]+)' => 'catalog/update/$1',
    'catalog/delete/([0-9]+)' => 'catalog/delete/$1',
    'catalog/create' => 'catalog/create',
    'catalog' => 'catalog/index',
    
    'publisher/update/([0-9]+)' => 'publisher/update/$1',
    'publisher/delete/([0-9]+)' => 'publisher/delete/$1',
    'publisher/create' => 'publisher/create',
    'publisher' => 'publisher/index',
    
    'about' => 'book/about',
    '' => 'book/index',
    
];
