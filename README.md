
ТЕСТОВОЕ ЗАДАНИЕ
----------------
    Тестовое задание не должно быть выполнено с помощью какого-либо фреймворка. Но можно использовать различные пакеты, установленные через compоser, включая компоненты Symfony.
    Для задания требуется спроектировать нормализованную структуру базы данных MySQL.
    Web приложение нужно выполнить таким образом, чтобы оно было переносимым между разными операционными системами. Т.е. приложение должно одинаково работать на сервере, развернутом Unix либо Windows.
    Web приложение должно работать при настройке php error_reporting(E_ALL);
    Рекомендуется продемонстрировать единый стиль обработки данных.
    Предусмотреть загрузку файлов разными способами (файловая система, удаленный файловый сервер) не изменяя логику работы приложения.
    При выполнении тестового задания рекомендуется выбрать готовый бесплатный сверстанный шаблон, мы хотим увидеть ваши способности по натягиванию дизайна и умению пользоваться поиском. В компании есть дизайнеры и верстальщики, и программисту не нужно рисовать или верстать, но должно быть продемонстрировано умение работать с готовым дизайном.
    Задание должно быть залито в любой публичный git репозиторий, база данных приложения должна развертываться с миграции.
    Будет плюсом ссылка на приложение, развернутое на каком-нибудь бесплатном хостинге.

Каталог книг

Исходные данные: название книги, фото книги, авторы книги, дата издательства, названия издательства, адрес издательства, телефон издательства, рубрика книги.

Для одной книги указывать разное количество фото, и сделать возможность просматривать эти фото.

Требуется реализовать следующие страницы:

    CRUD Издательств
    CRUD Авторов
    CRUD Книг
    CRUD Рубрик (рубрики должны быть иерархическими), например:
        История
            Мировая
            Средних веков
                Страны
                    Украина
                    США
        Программирование
            PHP
            JAVA

Установка
---------
1. Клонирование репозитория
git clone https://gapdn@bitbucket.org/gapdn/step_books.git

2. Настройка соединения с БД
добавить в проект файл db_params.php (каталог /config)

содержимое файла:

<?php

return [
    'user' => 'user_name',
    'password' => 'user_password',
    'host' => 'localhost',
    'dbname' => 'your_db_name',
];

3. Создание БД:


CREATE DATABASE `db_name` CHARACTER SET utf8 COLLATE utf8_general_ci;

USE db_name;

CREATE USER 'user_name'@'localhost' IDENTIFIED BY 'user_password';

GRANT ALL PRIVILEGES ON db_name.* TO 'user_name'@'localhost';

FLUSH PRIVILEGES;

CREATE TABLE `book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL, 
  `catalog_id` int(11) NOT NULL,
  `publisher_id` int(11) NOT NULL,
  `published_at` timestamp,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `book_to_author` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,  
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` varchar(255) NOT NULL,  
  `book_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `author` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `publisher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL, 
  `phone` varchar(15),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `catalog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `parent_catalog_id` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
------------------------------

Проект развернут на сервере с:
------------------------------
- Ubuntu 18.04
- PHP 7.2.7
- nginx/1.14.0
- MariaDB 6