<footer>                
            <div class="footer">
                <div class="back-to-top-page">
                    <a class="back-to-top"><i class="fa fa-angle-double-up"></i></a>
                </div>
                <p class="text">HOP for STEP | <?php echo date("Y"); ?></p>
            </div>
        </footer>
    </body>
</html>
<script>
    jQuery(document).ready(function ($) {


        /*====== BACK TO TOP ======*/
        $('.back-to-top-page').each(function () {
            $('.back-to-top').on('click', function (event) {
                event.preventDefault();
                $('html, body').animate({scrollTop: 0}, 1500);
                return false;
            });
        });

    });
</script>

