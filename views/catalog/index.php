<?php include_once VIEW.'/layouts/header.php'; ?>
            <div class="container full">
                <div class="page-posts no-padding">                    
                    <div class="row">                        
                        <div class="page page-post col-sm-12 col-xs-12">
                            <div class="blog-posts blog-posts-large">

                                <div class="row">
                                    <?php if (!empty($catalogs)): ?>
                                    <table class="table">
                                        <tr>
                                            <th>ID</th>                                                                                       
                                            <th>Имя</th>
                                            <th>Родительская рубрика</th>
                                            <th></th>
                                        </tr>
                                        <?php foreach ($catalogs as $catalog): ?>
                                        <tr>
                                            <td><?php echo $catalog['id']; ?></td>                                            
                                            <td><?php echo $catalog['title']; ?></td>                                                                                        
                                            <td><?= ($catalog['parent_catalog'] != '') ? $catalog['parent_catalog'] : ''; ?></td>                                                                                        
                                            <td>
                                                <a href="/catalog/update/<?php echo $catalog['id']; ?>"><span title="редактировать" class="glyphicon glyphicon-pencil"></span></a>
                                                <a href="/catalog/create" class="btn" ><span title="создать" class="glyphicon glyphicon-plus"></span></a>
                                                <a href="/catalog/delete/<?php echo $catalog['id']; ?>" class="btn" ><span title="удалить" class="glyphicon glyphicon-trash"></span></a>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </table>
                                    <?php else: ?>
                                        <div class="col-md-12">
                                            <?php echo $message ?>
                                            <a href="/catalog/create">Добавить рубрику</a>
                                        </div>
                                    <?php endif; ?>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div>
                <br>
            </div>
            <div class="push"></div>
        
<?php include_once VIEW.'/layouts/footer.php'; ?>








