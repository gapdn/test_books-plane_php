<div class="post-default-index">

    <h1>Обновить Рубрику</h1>
    <form method="post">
        <div class="form-group field-postform-name">
            <label class="control-label" for="postform-name">Имя Рубрики</label>
            <input value="<?= $catalog['title'] ?? '' ?>" type="text" name="name" class="form-control" id="postform-name" required="required"/>
            <div class="help-block"></div>
        </div>
        <div class="form-group field-postform-parrent">
            <label class="control-label" for="postform-parrent">Родительская рубрика</label>
            <select name="parent" class="form-control" id="postform-parrent" >
                <option>Корневой</option>
                <?php foreach ($catalogs as $catalog): ?>
                <option value="<?php echo $catalog['id'] ?>">
                    <?php echo $catalog['title'] ?>
                </option>
                <?php endforeach; ?>
            </select>
            <div class="help-block"></div>
        </div>        
        <input type="submit" value="<?php echo $button ?>"/>
    </form>

</div>