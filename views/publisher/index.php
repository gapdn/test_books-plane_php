<?php include_once VIEW.'/layouts/header.php'; ?>
            <div class="container full">
                <div class="page-posts no-padding">                    
                    <div class="row">                        
                        <div class="page page-post col-sm-12 col-xs-12">
                            <div class="blog-posts blog-posts-large">

                                <div class="row">
                                    <?php if (!empty($publishers)): ?>
                                    <table class="table">
                                        <tr>
                                            <th>ID</th>
                                            <th>Имя</th>                                            
                                            <th>Адрес</th>                                            
                                            <th>Телефон</th>                                            
                                            <th></th>
                                        </tr>
                                        <?php foreach ($publishers as $publisher): ?>
                                        <tr>
                                            <td><?php echo $publisher['id']; ?></td>
                                            <td><?php echo $publisher['name']; ?></td>
                                            <td><?php echo $publisher['address']; ?></td>
                                            <td><?php echo $publisher['phone']; ?></td>
                                            <td>
                                                <a href="/publisher/update/<?php echo $publisher['id']; ?>"><span title="редактировать" class="glyphicon glyphicon-pencil"></span></a>
                                                <a href="/publisher/create" class="btn" ><span title="создать" class="glyphicon glyphicon-plus"></span></a>
                                                <a href="/publisher/delete/<?php echo $publisher['id']; ?>" class="btn" ><span title="удалить" class="glyphicon glyphicon-trash"></span></a>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </table>
                                    <?php else: ?>
                                        <div class="col-md-12">
                                            <?php echo $message ?>
                                            <a href="/publisher/create">Добавить издательство</a>
                                        </div>
                                    <?php endif; ?>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div>
                <br>
            </div>
            <div class="push"></div>
        
<?php include_once VIEW.'/layouts/footer.php'; ?>








