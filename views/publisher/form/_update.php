<div class="post-default-index">

    <h1>Обновить Издательство</h1>
    <form method="post">
        <div class="form-group field-postform-name">
            <label class="control-label" for="postform-name">Имя</label>
            <input value="<?= $publisher['name'] ?>" type="text" name="name" class="form-control" id="postform-name" required="required"/>
            <div class="help-block"></div>
        </div>
        <div class="form-group field-postform-address">
            <label class="control-label" for="postform-address">Адрес</label>
            <input value="<?= $publisher['address'] ?>" type="text" name="address" class="form-control" id="postform-address" required="required"/>
            <div class="help-block"></div>
        </div>
        <div class="form-group field-postform-phone">
            <label class="control-label" for="postform-phone">Телефон</label>
            <input value="<?= $publisher['phone'] ?>" type="tel" name="phone" class="form-control" id="postform-phone" required="required"/>
            <div class="help-block"></div>
        </div>
        <input type="submit" value="<?php echo $button ?>"/>
    </form>

</div>
