<?php include_once VIEW.'/layouts/header.php'; ?>
            <div class="container full">
                <div class="page-posts no-padding">                    
                    <div class="row">                        
                        <div class="page page-post col-sm-12 col-xs-12">
                            <div class="blog-posts blog-posts-large">

                                <div class="row">
                                   
                                    <?php if (!empty($books)): ?>
                                    <table class="table">
                                        <tr>
                                            <th>Название</th>
                                            <th>Автор</th>
                                            <th>Рубрика</th>
                                            <th>Дата публикации</th>
                                            <th>Издательство</th>
                                            <th>Адрес</th>
                                            <th>Телефон</th>
                                            <th>Фото</th>
                                            <th></th>
                                        </tr>
                                        <?php foreach ($books as $book): ?>
                                        <tr>
                                            <td><?php echo $book['name']; ?></td>
                                            <td><?php echo implode(', ', $book['authors']); ?></td>
                                            <td><?php echo $book['catalog']; ?></td>
                                            <td><?php echo strftime('%d-%m-%Y', strtotime($book['date_published'])); ?></td>
                                            <td><?php echo $book['publisher'][0]['name']; ?></td>
                                            <td><?php echo $book['publisher'][0]['address']; ?></td>
                                            <td><?php echo $book['publisher'][0]['phone']; ?></td>
                                            <?php// if($book['photo']): ?>
                                            <td><img src="<?php echo $book['photo']; ?>" alt="photo"/></td>
                                            <?php// endif; ?>
                                            <td>
                                                <a href="/book/update/<?php echo $book['id'] ?>"><span class="glyphicon glyphicon-pencil"></span></a><br/>
                                                <a href="/book/create"><span class="glyphicon glyphicon-plus"></span></a><br/>
                                                <a href="/book/delete/<?php echo $book['id']; ?>" class="btn" ><span title="удалить" class="glyphicon glyphicon-trash"></span></a>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?> 
                                    </table>
                                    <?php else: ?>
                                        <div class="col-md-12">
                                            <?php echo $message ?>
                                            <a href="/book/create">Добавить книгу</a>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <br>
            </div>
            <div class="push"></div>
        </div>
<?php include_once VIEW.'/layouts/footer.php'; ?>








