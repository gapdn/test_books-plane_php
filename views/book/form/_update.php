<div class="post-default-index">

    <h1>Обновить Книгу</h1>
    <form method="post" enctype="multipart/form-data">
        <div class="form-group field-postform-name">
            <label class="control-label" for="postform-name">Название</label>
            <input value="<?= $book['name'] ? $book['name'] : '' ?>" type="text" name="name" class="form-control" id="postform-name" required="required">
            <div class="help-block"></div>
        </div>
        <div class="form-group field-postform-author">
            <label class="control-label" for="postform-author">Автор</label>
            <select multiple name="author[]" id="postform-author" class="form-control" required="required">
                <?php foreach ($authors as $author): ?>
                <option <?= (in_array($author['id'], $checked_ids) ? 'selected' : '') ?> value="<?= $author['id'] ?>"><?= $author['name'] ?></option>
                <?php endforeach; ?>
            </select>
            <div class="help-block"></div>
        </div>
        <div class="form-group field-postform-date">
            <label class="control-label" for="postform-date">Дата публикации</label>
            <input value="<?= $book['published_at'] ? strftime('%Y-%m-%d', strtotime($book['published_at'])) : '' ?>" type="date" name="date"  id="postform-date" required="required">
            <div class="help-block"></div>
        </div>
        <div class="form-group field-postform-publisher">
            <label class="control-label" for="postform-publisher">Издательство</label>
            <select name="publisher" class="form-control" id="postform-author" required="required">
                <?php foreach ($publishers as $publisher): ?>
                <option value="<?php echo $publisher['id']; ?>"><?php echo $publisher['name']; ?></option>
                <?php endforeach; ?>
            </select>
            <div class="help-block"></div>
        </div>
        <div class="form-group field-postform-catalog">
            <label class="control-label" for="postform-catalog">Рубрика</label>
            <select name="catalog" class="form-control" id="postform-catalog" required="required">
                <?php foreach ($catalogs as $catalog): ?>
                <option value="<?php echo $catalog['id']; ?>"><?php echo $catalog['title']; ?></option>
                <?php endforeach; ?>
            </select>
            <div class="help-block"></div>
        </div>
        <div class="form-group field-postform-photo">
            <label class="control-label" for="postform-photo">Фото</label>
            <input type="file" name="photo" enctype="multipart/form-data" id="postform-photo" />
            <div class="help-block"></div>
        </div>
        <input name="submit" type="submit" class="btn btn-default" value="<?php echo $button ?>"/>
    </form>

</div>