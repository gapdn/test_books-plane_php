<?php include_once VIEW.'/layouts/header.php'; ?>
            <div class="container full">
                <div class="page-posts no-padding">                    
                    <div class="row">                        
                        <div class="page page-post col-sm-12 col-xs-12">
                            <div class="blog-posts blog-posts-large">

                                <div class="row">
                                    <?php if(!isset($book)) {
                                        include_once VIEW . '/book/form/_create.php';
                                    } else {
                                        include_once VIEW . '/book/form/_update.php';
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <br>
            </div>
            <div class="push"></div>
        </div>
<?php include_once VIEW.'/layouts/footer.php'; ?>








