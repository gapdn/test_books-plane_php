<div class="post-default-index">

    <h1>Обновить Автора</h1>
    <form method="post">
        <div class="form-group field-postform-name">
            <label class="control-label" for="postform-name">Автор</label>
            <input value="<?= $author['name'] ? $author['name'] : '' ?>" type="text" name="name" class="form-control" id="postform-name" required="required"/>
            <div class="help-block"></div>
        </div>
        <input type="submit" value="<?php echo $button ?>"/>
    </form>

</div>
