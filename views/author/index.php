<?php include_once VIEW.'/layouts/header.php'; ?>
            <div class="container full">
                <div class="page-posts no-padding">                    
                    <div class="row">                        
                        <div class="page page-post col-sm-12 col-xs-12">
                            <div class="blog-posts blog-posts-large">

                                <div class="row">
<?php if (!empty($authors)): ?>
                                    <table class="table">
                                        <tr>
                                            <th>ID</th>
                                            <th>Имя</th>                                            
                                            <th></th>
                                        </tr>
                                        <?php foreach ($authors as $author): ?>
                                        <tr>
                                            <td><?php echo $author['id']; ?></td>
                                            <td><?php echo $author['name']; ?></td>
                                            <td>
                                                <a href="/author/update/<?php echo $author['id']; ?>"><span title="редактировать" class="glyphicon glyphicon-pencil"></span></a>
                                                <a href="/author/create" class="btn" ><span title="создать" class="glyphicon glyphicon-plus"></span></a>
                                                <a href="/author/delete/<?php echo $author['id']; ?>" class="btn" ><span title="удалить" class="glyphicon glyphicon-trash"></span></a>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </table>
                                    <?php else: ?>
                                        <div class="col-md-12">
                                            <?php echo $message ?>
                                            <a href="/author/create">Добавить автора</a>
                                        </div>
                                    <?php endif; ?>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div>
                <br>
            </div>
            <div class="push"></div>
        
<?php include_once VIEW.'/layouts/footer.php'; ?>








