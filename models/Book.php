<?php

require_once (ROOT . 'models/AppModel.php');

class Book extends AppModel {

    public function __construct() {
        parent::__construct();
        $this->table_name = 'book';
    }
    
    public function getId($name) {
        $sql = "SELECT * FROM $this->table_name WHERE name = :name";
        $book = $this->db->query($sql, [
            ':name' => $name,
        ]);
        
        if (!empty($book)) {
            return $book[0]['id'];
        }
        
        return  null;
    }
    
    public function getBooks() {

        $sql = "SELECT * FROM $this->table_name order by id desc limit 100";
        $books = $this->db->query($sql);
        return $books;
    }
    
    public function getPhoto($id) {
        
        $sql = 'SELECT * FROM photo WHERE book_id = :id';
        $photo = $this->db->query($sql, [
            ':id' => $id,
        ]);
        return $photo;
    }
    
    public function getCatalogName($id) {
        
        $sql = 'SELECT * FROM catalog WHERE id = :id';
        $catalog = $this->db->query($sql, [
            ':id' => $id,
        ]);
        
        return $catalog[0]['title'];
    }
    
    public function getPublisher($id) {
        
        $sql = 'SELECT * from publisher WHERE id = :id';
        $publisher = $this->db->query($sql, [
            ':id' => $id,
        ]);
        
        return $publisher;
    }
    
    public function getBookById($id) {
        
        if (empty($id)) { 
            return [];
        }
        $sql = "SELECT * from $this->table_name WHERE id = :id";
        $result = $this->db->query($sql, [':id' => $id]);
        
        return $result[0];
    }
    
    public function getAuthorsName($book_id) {
        
        $sql = 'SELECT author.* FROM book_to_author LEFT JOIN author ON author.id = book_to_author.author_id WHERE book_id = :id';
        $result = $this->db->query($sql, [':id' => $book_id]);
        $names = [];
        foreach ($result as $row) {
            $names[] = $row['name'];
        }
        
        return $names;
    }
    
    public function getAuthorsId($book_id) {
        
        $sql = 'SELECT author.* FROM book_to_author LEFT JOIN author ON author.id = book_to_author.author_id WHERE book_id = :id';
        $result = $this->db->query($sql, [':id' => $book_id]);
        $ids = [];
        foreach ($result as $row) {
            $ids[] = $row['id'];
        }
        
        return $ids;
    }
    
    public function create($name, $catalog_id, $publisher_id, $published_at) {
        
        $sql = "INSERT INTO $this->table_name (name, catalog_id, publisher_id, published_at) "
                . "VALUES (:name, :catalog_id, :publisher_id, :published_at)";
        $result = $this->db->query($sql, [
            ':name' => $name,
            ':catalog_id' => $catalog_id,
            ':publisher_id' => $publisher_id,
            ':published_at' => $published_at,
        ]);
        
        return $result;
    }
    
    public function update($id, $name, $catalog_id, $publisher_id, $published_at) {
        
        $sql = "UPDATE $this->table_name SET name=:name, catalog_id=:catalog_id, publisher_id=:publisher_id, published_at=:published_at "
                . "WHERE id=:id";
        $result = $this->db->query($sql, [
            ':id' => $id,
            ':name' => $name,
            ':catalog_id' => $catalog_id,
            ':publisher_id' => $publisher_id,
            ':published_at' => $published_at,
        ]);
        
        return $result;
    }
    
    public function delete($id) {
        
        $sql = "DELETE FROM $this->table_name  WHERE id=:id";
        $result = $this->db->query($sql, [':id' => $id]);
        
        return $result;
    }
    
    public function read($id) {
        
        $sql = "SELECT * FROM $this->table_name WHERE id=:id";
        $result = $this->db->query($sql, [':id' => $id]);
        
        return $result;
    }
}
