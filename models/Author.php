<?php

require_once (ROOT . 'models/AppModel.php');

class Author extends AppModel{
    
    public function __construct() {
        parent::__construct();
        $this->table_name = 'author';
    }
    
    public function getId($name) {
        
        $sql = "SELECT * from $this->table_name WHERE name = :name";
        $author = $this->db->query($sql, [':name' => $name]);
        if (!empty($author)) {           
            return $author[0]['id'];
        }
        
        return null;
    }
    
    public function getName($id) {

        $sql = "SELECT name from $this->table_name WHERE id = :id";
        $author = $this->db->query($sql, [':id' => $id]);
        
        if (!empty($author)) {
            $name = $author[0]['name'];
            return $name;
        }
        
        return null;
    }
    
    public function getAuthors() {
        $sql = "SELECT * from $this->table_name";
        $authors = $this->db->query($sql);
        
        if (!empty($authors)) {
            return $authors;
        }
        
        return null;
    }

    public function getAuthorById($id) {
        
        if (empty($id)) { 
            return [];
        }
        $sql = "SELECT * from $this->table_name WHERE id = :id";
        $result = $this->db->query($sql, [':id' => $id]);
        
        return $result[0];
    }
    
    public function create($name) {
        
        $sql = "INSERT INTO $this->table_name (name) VALUES (:name)";
        $result = $this->db->query($sql, [':name' => $name]);
        
        return $result;
    }
    
    public function update($id, $name) {
        
        $sql = "UPDATE $this->table_name SET name=:name WHERE id=:id";
        $result = $this->db->query($sql, [':name' => $name, ':id' => $id]);
        
        return $result;
    }
    
    public function delete($id) {
        
        $sql = "DELETE FROM $this->table_name  WHERE id=:id";
        $result = $this->db->query($sql, [':id' => $id]);
        
        return $result;
    }
    
    public function read($id) {
        
        $sql = "SELECT * FROM $this->table_name WHERE id=:id";
        $result = $this->db->query($sql, [':id' => $id]);
        
        return $result;
    }
}
