<?php

require_once (ROOT . 'models/AppModel.php');

class BookToAuthor extends AppModel{
    
    public function __construct() {
        parent::__construct();
        $this->table_name = 'book_to_author';
    }
    
    public function getId($book_id, $author_id) {
        
        $sql = "SELECT * from $this->table_name WHERE book_id = :book_id AND author_id = :author_id";
        $result = $this->db->query($sql, [
            ':book_id' => $book_id,
            ':author_id' => $author_id,
        ]);
        if (!empty($result)) {
            $id = $result[0]['id'];
            return $id;
        }
        
        return null;
    }
    
    public function getBookToAuthors($book_id) {
        
        $sql = "SELECT * from $this->table_name WHERE book_id = :book_id";
        $result = $this->db->query($sql, [
            ':book_id' => $book_id,
        ]);
        if (!empty($result)) {
            return $result;
        }
        
        return null;
    }

    public function isExists($book_id, $author_id) {
        $sql = "SELECT * from $this->table_name WHERE book_id = :book_id AND author_id = :author_id";
        $result = $this->db->query($sql, [
            ':book_id' => $book_id,
            ':author_id' => $author_id,
        ]);
        
        if (empty($result)) {
            return false;
        }
        
        return true;
    }
    
    public function create($book_id, $author_id) {
                
        $sql = "INSERT INTO $this->table_name (book_id, author_id) VALUES (:book_id, :author_id)";
        $result = $this->db->query($sql, [
            ':book_id' => $book_id,
            ':author_id' => $author_id,
        ]);
        
        return $result;
    }
    
    public function delete($id) {
        
        $sql = "DELETE FROM $this->table_name WHERE id = :id";
        $result = $this->db->query($sql, [
            ':id' => $id,
           
        ]);
        
        return $result;
    }
}
