<?php

require_once (ROOT . 'models/AppModel.php');

class Photo extends AppModel{
    
    public function __construct() {
        parent::__construct();
        $this->table_name = 'photo';
    }
    
    public function getPath($book_id) {
        $sql = "SELECT * FROM $this->table_name WHERE book_id = :book_id";
        $result = $this->db->query($sql, [
            ':book_id' => $book_id,
        ]);
        
        return $result[0]['path'];
    }
    
    public function preparePath($path) {
        $result = substr(strrchr($path, '/'), 1);
        return '/uploads/' . $result;
    }
    
    public function create($path, $book_id) {
        
        $sql = "INSERT INTO $this->table_name (path, book_id) VALUES (:path, :book_id)";
        $result = $this->db->query($sql, [
            ':path' => $path,
            ':book_id' => $book_id,
        ]);
        
        return $result;
    }
    
    public function update($id, $path) {
        
        $sql = "UPDATE $this->table_name SET path=:path WHERE id=:id";
        $result = $this->db->query($sql, [
            ':id' => $id,
            ':path' => $path,
        ]);
        
        return $result;
    }
    
    public function delete($book_id) {
        
        $sql = "DELETE FROM $this->table_name WHERE book_id=:id";
        $result = $this->db->query($sql, [
            ':id' => $book_id,
        ]);
        
        return $result;
    }
    
    public function upload($file) {

        $target_dir = UPLOAD_DIR;
        $target_file = $target_dir . basename($file["name"]);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
        if (isset($_POST["submit"])) {
            $check = getimagesize($file["tmp_name"]);
            if ($check !== false) {
                echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                echo "File is not an image.";
                $uploadOk = 0;
            }
        }
// Check if file already exists
        if (file_exists($target_file)) {
            
            $uploadOk = 0;
        }
// Check file size
        if ($file["size"] > MAX_FILE_SIZE) {
            
            $uploadOk = 0;
        }
// Allow certain file formats
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
            
            $uploadOk = 0;
        }
// Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            
            return 0;
// if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($file["tmp_name"], $target_file)) {               
                return $target_file;
            } else {
                
                return 0;
            }
        }
    }

}
