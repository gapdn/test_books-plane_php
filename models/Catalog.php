<?php

require_once (ROOT . 'models/AppModel.php');

class Catalog extends AppModel{
    
    public function __construct() {
        parent::__construct();
        $this->table_name = 'catalog';
    }
    
    public function getId($name) {
        
        $sql = "SELECT id from $this->table_name WHERE name = :name";
        $author = $this->db->query($sql, [':name' => $name]);
        if (!empty($author)) {
            $id = $author[0]['id'];
            return $id;
        }
        
        return null;
    }
    
    public function getName($id) {

        $sql = "SELECT title from $this->table_name WHERE id = :id";
        $catalog = $this->db->query($sql, [':id' => $id]);
        
        if (!empty($catalog)) {            
            $name = $catalog[0]['title'];
            
            return $name;
        }
        
        return null;
    }
    
    public function getCatalogs() {
        $sql = "SELECT * FROM $this->table_name";
        $result = $this->db->query($sql);
        
        return $result;
    }
    
    public function getCatalogById($id) {
        
        if (empty($id)) { 
            return [];
        }
        $sql = "SELECT * from $this->table_name WHERE id = :id";
        $result = $this->db->query($sql, [':id' => $id]);
        
        return $result[0];
    }
    
    public function create($title, $parent_catalog_id = 0) {
        
        
        
        $sql = "INSERT INTO $this->table_name (title, parent_catalog_id) VALUES (:title, :parent_catalog_id)";
        $result = $this->db->query($sql, [
            ':title' => $title,
            ':parent_catalog_id' => $parent_catalog_id,
        ]);
        
        return $result;
    }
    
    public function update($id, $title, $parent_catalog_id) {
        
        $sql = "UPDATE $this->table_name SET title=:title, parent_catalog_id=:parent_catalog_id WHERE id=:id";
        $result = $this->db->query($sql, [
            ':title' => $title,
            ':parent_catalog_id' => $parent_catalog_id,
            ':id' => $id,
        ]);
        
        return $result;
    }
    
    public function delete($id) {
        
        $sql = "DELETE FROM $this->table_name  WHERE id=:id";
        $result = $this->db->query($sql, [':id' => $id]);
        
        return $result;
    }
    
    public function read($id) {
        
        $sql = "SELECT * FROM $this->table_name WHERE id=:id";
        $result = $this->db->query($sql, [':id' => $id]);
        
        return $result;
    }
}
