<?php

require_once (ROOT . 'models/AppModel.php');

class Publisher extends AppModel{
    
    public function __construct() {
        parent::__construct();
        $this->table_name = 'publisher';
    }
    
    public function getPublishers() {
        
        $sql = "SELECT * FROM $this->table_name";
        $result = $this->db->query($sql);
        
        return $result;
    }

        public function create($name, $address, $phone) {
        
        $sql = "INSERT INTO $this->table_name (name, address, phone) VALUES (:name, :address, :phone)";
        $result = $this->db->query($sql, [
            ':name' => $name,
            ':address' => $address,
            ':phone' => $phone,
        ]);
        
        return $result;
    }
    
    public function getPublisherById($id) {
        
        if (empty($id)) { 
            return [];
        }
        $sql = "SELECT * from $this->table_name WHERE id = :id";
        $result = $this->db->query($sql, [':id' => $id]);
        
        return $result[0];
    }
    
    public function update($id, $name, $address, $phone) {
        
        $sql = "UPDATE $this->table_name SET name=:name, address=:address, phone=:phone WHERE id=:id";
        $result = $this->db->query($sql, [
            ':name' => $name,
            ':address' => $address,
            ':phone' => $phone,
            ':id' => $id,
        ]);
        
        return $result;
    }
    
    public function delete($id) {
        
        $sql = "DELETE FROM $this->table_name  WHERE id=:id";
        $result = $this->db->query($sql, [':id' => $id]);
        
        return $result;
    }
    
    public function read($id) {
        
        $sql = "SELECT * FROM $this->table_name WHERE id=:id";
        $result = $this->db->query($sql, [':id' => $id]);
        
        return $result;
    }
}
